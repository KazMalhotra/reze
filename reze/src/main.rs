#![allow(dead_code,
         mutable_transmutes,
         non_camel_case_types,
         non_snake_case,
         non_upper_case_globals,
         unused_assignments,
         unused_mut)]
#![feature(c_variadic, const_raw_ptr_to_usize_cast, ptr_wrapping_offset_from)]
extern crate libc;
extern "C" {
    #[no_mangle]
    fn tcgetattr(__fd: libc::c_int, __termios_p: *mut termios) -> libc::c_int;
    #[no_mangle]
    fn tcsetattr(__fd: libc::c_int, __optional_actions: libc::c_int,
                 __termios_p: *const termios) -> libc::c_int;
    #[no_mangle]
    fn malloc(_: libc::c_ulong) -> *mut libc::c_void;
    #[no_mangle]
    fn realloc(_: *mut libc::c_void, _: libc::c_ulong) -> *mut libc::c_void;
    #[no_mangle]
    fn free(__ptr: *mut libc::c_void);
    #[no_mangle]
    fn atexit(__func: Option<unsafe extern "C" fn() -> ()>) -> libc::c_int;
    #[no_mangle]
    fn exit(_: libc::c_int) -> !;
    #[no_mangle]
    static mut stderr: *mut _IO_FILE;
    #[no_mangle]
    fn fclose(__stream: *mut FILE) -> libc::c_int;
    #[no_mangle]
    fn fopen(__filename: *const libc::c_char, __modes: *const libc::c_char)
     -> *mut FILE;
    #[no_mangle]
    fn fprintf(_: *mut FILE, _: *const libc::c_char, _: ...) -> libc::c_int;
    #[no_mangle]
    fn snprintf(_: *mut libc::c_char, _: libc::c_ulong,
                _: *const libc::c_char, _: ...) -> libc::c_int;
    #[no_mangle]
    fn vsnprintf(_: *mut libc::c_char, _: libc::c_ulong,
                 _: *const libc::c_char, _: ::std::ffi::VaList)
     -> libc::c_int;
    #[no_mangle]
    fn sscanf(_: *const libc::c_char, _: *const libc::c_char, _: ...)
     -> libc::c_int;
    #[no_mangle]
    fn getline(__lineptr: *mut *mut libc::c_char, __n: *mut size_t,
               __stream: *mut FILE) -> __ssize_t;
    #[no_mangle]
    fn perror(__s: *const libc::c_char);
    #[no_mangle]
    fn __errno_location() -> *mut libc::c_int;
    #[no_mangle]
    fn memcpy(_: *mut libc::c_void, _: *const libc::c_void, _: libc::c_ulong)
     -> *mut libc::c_void;
    #[no_mangle]
    fn memmove(_: *mut libc::c_void, _: *const libc::c_void, _: libc::c_ulong)
     -> *mut libc::c_void;
    #[no_mangle]
    fn memset(_: *mut libc::c_void, _: libc::c_int, _: libc::c_ulong)
     -> *mut libc::c_void;
    #[no_mangle]
    fn memcmp(_: *const libc::c_void, _: *const libc::c_void,
              _: libc::c_ulong) -> libc::c_int;
    #[no_mangle]
    fn strdup(_: *const libc::c_char) -> *mut libc::c_char;
    #[no_mangle]
    fn strchr(_: *const libc::c_char, _: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn strstr(_: *const libc::c_char, _: *const libc::c_char)
     -> *mut libc::c_char;
    #[no_mangle]
    fn strlen(_: *const libc::c_char) -> libc::c_ulong;
    #[no_mangle]
    fn strerror(_: libc::c_int) -> *mut libc::c_char;
    #[no_mangle]
    fn __ctype_b_loc() -> *mut *const libc::c_ushort;
    #[no_mangle]
    fn ioctl(__fd: libc::c_int, __request: libc::c_ulong, _: ...)
     -> libc::c_int;
    #[no_mangle]
    fn close(__fd: libc::c_int) -> libc::c_int;
    #[no_mangle]
    fn read(__fd: libc::c_int, __buf: *mut libc::c_void, __nbytes: size_t)
     -> ssize_t;
    #[no_mangle]
    fn write(__fd: libc::c_int, __buf: *const libc::c_void, __n: size_t)
     -> ssize_t;
    #[no_mangle]
    fn isatty(__fd: libc::c_int) -> libc::c_int;
    #[no_mangle]
    fn ftruncate(__fd: libc::c_int, __length: __off_t) -> libc::c_int;
    #[no_mangle]
    fn open(__file: *const libc::c_char, __oflag: libc::c_int, _: ...)
     -> libc::c_int;
}
pub type __builtin_va_list = [__va_list_tag; 1];
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct __va_list_tag {
    pub gp_offset: libc::c_uint,
    pub fp_offset: libc::c_uint,
    pub overflow_arg_area: *mut libc::c_void,
    pub reg_save_area: *mut libc::c_void,
}
pub type __off_t = libc::c_long;
pub type __off64_t = libc::c_long;
pub type __time_t = libc::c_long;
pub type __ssize_t = libc::c_long;
pub type cc_t = libc::c_uchar;
pub type speed_t = libc::c_uint;
pub type tcflag_t = libc::c_uint;
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct termios {
    pub c_iflag: tcflag_t,
    pub c_oflag: tcflag_t,
    pub c_cflag: tcflag_t,
    pub c_lflag: tcflag_t,
    pub c_line: cc_t,
    pub c_cc: [cc_t; 32],
    pub c_ispeed: speed_t,
    pub c_ospeed: speed_t,
}
pub type size_t = libc::c_ulong;
pub type ssize_t = __ssize_t;
pub type time_t = __time_t;
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct _IO_FILE {
    pub _flags: libc::c_int,
    pub _IO_read_ptr: *mut libc::c_char,
    pub _IO_read_end: *mut libc::c_char,
    pub _IO_read_base: *mut libc::c_char,
    pub _IO_write_base: *mut libc::c_char,
    pub _IO_write_ptr: *mut libc::c_char,
    pub _IO_write_end: *mut libc::c_char,
    pub _IO_buf_base: *mut libc::c_char,
    pub _IO_buf_end: *mut libc::c_char,
    pub _IO_save_base: *mut libc::c_char,
    pub _IO_backup_base: *mut libc::c_char,
    pub _IO_save_end: *mut libc::c_char,
    pub _markers: *mut _IO_marker,
    pub _chain: *mut _IO_FILE,
    pub _fileno: libc::c_int,
    pub _flags2: libc::c_int,
    pub _old_offset: __off_t,
    pub _cur_column: libc::c_ushort,
    pub _vtable_offset: libc::c_schar,
    pub _shortbuf: [libc::c_char; 1],
    pub _lock: *mut libc::c_void,
    pub _offset: __off64_t,
    pub __pad1: *mut libc::c_void,
    pub __pad2: *mut libc::c_void,
    pub __pad3: *mut libc::c_void,
    pub __pad4: *mut libc::c_void,
    pub __pad5: size_t,
    pub _mode: libc::c_int,
    pub _unused2: [libc::c_char; 20],
}
pub type _IO_lock_t = ();
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct _IO_marker {
    pub _next: *mut _IO_marker,
    pub _sbuf: *mut _IO_FILE,
    pub _pos: libc::c_int,
}
pub type FILE = _IO_FILE;
pub type va_list = __builtin_va_list;
pub type C2RustUnnamed = libc::c_uint;
pub const _ISalnum: C2RustUnnamed = 8;
pub const _ISpunct: C2RustUnnamed = 4;
pub const _IScntrl: C2RustUnnamed = 2;
pub const _ISblank: C2RustUnnamed = 1;
pub const _ISgraph: C2RustUnnamed = 32768;
pub const _ISprint: C2RustUnnamed = 16384;
pub const _ISspace: C2RustUnnamed = 8192;
pub const _ISxdigit: C2RustUnnamed = 4096;
pub const _ISdigit: C2RustUnnamed = 2048;
pub const _ISalpha: C2RustUnnamed = 1024;
pub const _ISlower: C2RustUnnamed = 512;
pub const _ISupper: C2RustUnnamed = 256;
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct winsize {
    pub ws_row: libc::c_ushort,
    pub ws_col: libc::c_ushort,
    pub ws_xpixel: libc::c_ushort,
    pub ws_ypixel: libc::c_ushort,
}
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct editorSyntax {
    pub filematch: *mut *mut libc::c_char,
    pub keywords: *mut *mut libc::c_char,
    pub singleline_comment_start: [libc::c_char; 2],
    pub multiline_comment_start: [libc::c_char; 3],
    pub multiline_comment_end: [libc::c_char; 3],
    pub flags: libc::c_int,
}
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct erow {
    pub idx: libc::c_int,
    pub size: libc::c_int,
    pub rsize: libc::c_int,
    pub chars: *mut libc::c_char,
    pub render: *mut libc::c_char,
    pub hl: *mut libc::c_uchar,
    pub hl_oc: libc::c_int,
}
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct editorConfig {
    pub cx: libc::c_int,
    pub cy: libc::c_int,
    pub rowoff: libc::c_int,
    pub coloff: libc::c_int,
    pub screenrows: libc::c_int,
    pub screencols: libc::c_int,
    pub numrows: libc::c_int,
    pub rawmode: libc::c_int,
    pub row: *mut erow,
    pub dirty: libc::c_int,
    pub filename: *mut libc::c_char,
    pub statusmsg: [libc::c_char; 80],
    pub statusmsg_time: time_t,
    pub syntax: *mut editorSyntax,
}
pub type KEY_ACTION = libc::c_uint;
pub const PAGE_DOWN: KEY_ACTION = 1008;
pub const PAGE_UP: KEY_ACTION = 1007;
pub const END_KEY: KEY_ACTION = 1006;
pub const HOME_KEY: KEY_ACTION = 1005;
pub const DEL_KEY: KEY_ACTION = 1004;
pub const ARROW_DOWN: KEY_ACTION = 1003;
pub const ARROW_UP: KEY_ACTION = 1002;
pub const ARROW_RIGHT: KEY_ACTION = 1001;
pub const ARROW_LEFT: KEY_ACTION = 1000;
pub const BACKSPACE: KEY_ACTION = 127;
pub const ESC: KEY_ACTION = 27;
pub const CTRL_U: KEY_ACTION = 21;
pub const CTRL_S: KEY_ACTION = 19;
pub const CTRL_Q: KEY_ACTION = 17;
pub const ENTER: KEY_ACTION = 13;
pub const CTRL_L: KEY_ACTION = 12;
pub const TAB: KEY_ACTION = 9;
pub const CTRL_H: KEY_ACTION = 8;
pub const CTRL_F: KEY_ACTION = 6;
pub const CTRL_D: KEY_ACTION = 4;
pub const CTRL_C: KEY_ACTION = 3;
pub const KEY_NULL: KEY_ACTION = 0;
#[derive ( Copy , Clone )]
#[repr(C)]
pub struct abuf {
    pub b: *mut libc::c_char,
    pub len: libc::c_int,
}
static mut E: editorConfig =
    editorConfig{cx: 0,
                 cy: 0,
                 rowoff: 0,
                 coloff: 0,
                 screenrows: 0,
                 screencols: 0,
                 numrows: 0,
                 rawmode: 0,
                 row: 0 as *const erow as *mut erow,
                 dirty: 0,
                 filename: 0 as *const libc::c_char as *mut libc::c_char,
                 statusmsg: [0; 80],
                 statusmsg_time: 0,
                 syntax: 0 as *const editorSyntax as *mut editorSyntax,};
#[no_mangle]
pub unsafe extern "C" fn clearScreen() {
    let mut CLEAR_SCREEN_ANSI: *const libc::c_char =
        b"\x1b[1;1H\x1b[2J\x00" as *const u8 as *const libc::c_char;
    write(1i32, CLEAR_SCREEN_ANSI as *const libc::c_void, 12i32 as size_t);
}
#[no_mangle]
pub static mut C_HL_extensions: [*mut libc::c_char; 3] =
    [b".c\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b".cpp\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     0 as *const libc::c_char as *mut libc::c_char];
#[no_mangle]
pub static mut C_HL_keywords: [*mut libc::c_char; 23] =
    [b"switch\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"if\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"while\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"for\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"break\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"continue\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"return\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"else\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"struct\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"union\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"typedef\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"static\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"enum\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"class\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"int|\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"long|\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"double|\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"float|\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"char|\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"unsigned|\x00" as *const u8 as *const libc::c_char as
         *mut libc::c_char,
     b"signed|\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     b"void|\x00" as *const u8 as *const libc::c_char as *mut libc::c_char,
     0 as *const libc::c_char as *mut libc::c_char];
#[no_mangle]
pub static mut HLDB: [editorSyntax; 1] =
    unsafe {
        [{
             let mut init =
                 editorSyntax{filematch: C_HL_extensions.as_ptr() as *mut _,
                              keywords: C_HL_keywords.as_ptr() as *mut _,
                              singleline_comment_start: [47, 47],
                              multiline_comment_start: [47, 42, 0],
                              multiline_comment_end: [42, 47, 0],
                              flags: 1i32 << 0i32 | 1i32 << 1i32,};
             init
         }]
    };
static mut orig_termios: termios =
    termios{c_iflag: 0,
            c_oflag: 0,
            c_cflag: 0,
            c_lflag: 0,
            c_line: 0,
            c_cc: [0; 32],
            c_ispeed: 0,
            c_ospeed: 0,};
#[no_mangle]
pub unsafe extern "C" fn disableRawMode(mut fd: libc::c_int) {
    if E.rawmode != 0 {
        tcsetattr(fd, 2i32, &mut orig_termios);
        E.rawmode = 0i32
    };
}
#[no_mangle]
pub unsafe extern "C" fn editorAtExit() { disableRawMode(0i32); }
#[no_mangle]
pub unsafe extern "C" fn enableRawMode(mut fd: libc::c_int) -> libc::c_int {
    let mut raw: termios =
        termios{c_iflag: 0,
                c_oflag: 0,
                c_cflag: 0,
                c_lflag: 0,
                c_line: 0,
                c_cc: [0; 32],
                c_ispeed: 0,
                c_ospeed: 0,};
    if E.rawmode != 0 { return 0i32 }
    if !(isatty(0i32) == 0) {
        atexit(Some(editorAtExit as unsafe extern "C" fn() -> ()));
        if !(tcgetattr(fd, &mut orig_termios) == -1i32) {
            raw = orig_termios;
            raw.c_iflag &=
                !(0o2i32 | 0o400i32 | 0o20i32 | 0o40i32 | 0o2000i32) as
                    libc::c_uint;
            raw.c_oflag &= !0o1i32 as libc::c_uint;
            raw.c_cflag |= 0o60i32 as libc::c_uint;
            raw.c_lflag &=
                !(0o10i32 | 0o2i32 | 0o100000i32 | 0o1i32) as libc::c_uint;
            raw.c_cc[6] = 0i32 as cc_t;
            raw.c_cc[5] = 1i32 as cc_t;
            if !(tcsetattr(fd, 2i32, &mut raw) < 0i32) {
                E.rawmode = 1i32;
                return 0i32
            }
        }
    }
    *__errno_location() = 25i32;
    return -1i32;
}
#[no_mangle]
pub unsafe extern "C" fn editorReadKey(mut fd: libc::c_int) -> libc::c_int {
    let mut nread: libc::c_int = 0;
    let mut c: libc::c_char = 0;
    let mut seq: [libc::c_char; 3] = [0; 3];
    loop  {
        nread =
            read(fd, &mut c as *mut libc::c_char as *mut libc::c_void,
                 1i32 as size_t) as libc::c_int;
        if !(nread == 0i32) { break ; }
    }
    if nread == -1i32 { exit(1i32); }
    loop  {
        match c as libc::c_int {
            27 => {
                if read(fd, seq.as_mut_ptr() as *mut libc::c_void,
                        1i32 as size_t) == 0i32 as libc::c_long {
                    return ESC as libc::c_int
                }
                if read(fd, seq.as_mut_ptr().offset(1) as *mut libc::c_void,
                        1i32 as size_t) == 0i32 as libc::c_long {
                    return ESC as libc::c_int
                }
                if seq[0] as libc::c_int == '[' as i32 {
                    if seq[1] as libc::c_int >= '0' as i32 &&
                           seq[1] as libc::c_int <= '9' as i32 {
                        if read(fd,
                                seq.as_mut_ptr().offset(2) as
                                    *mut libc::c_void, 1i32 as size_t) ==
                               0i32 as libc::c_long {
                            return ESC as libc::c_int
                        }
                        if seq[2] as libc::c_int == '~' as i32 {
                            match seq[1] as libc::c_int {
                                51 => { return DEL_KEY as libc::c_int }
                                53 => { return PAGE_UP as libc::c_int }
                                54 => { return PAGE_DOWN as libc::c_int }
                                _ => { }
                            }
                        }
                    } else {
                        match seq[1] as libc::c_int {
                            65 => { return ARROW_UP as libc::c_int }
                            66 => { return ARROW_DOWN as libc::c_int }
                            67 => { return ARROW_RIGHT as libc::c_int }
                            68 => { return ARROW_LEFT as libc::c_int }
                            72 => { return HOME_KEY as libc::c_int }
                            70 => { return END_KEY as libc::c_int }
                            _ => { }
                        }
                    }
                } else if seq[0] as libc::c_int == 'O' as i32 {
                    match seq[1] as libc::c_int {
                        72 => { return HOME_KEY as libc::c_int }
                        70 => { return END_KEY as libc::c_int }
                        _ => { }
                    }
                }
            }
            _ => { return c as libc::c_int }
        }
    };
}
#[no_mangle]
pub unsafe extern "C" fn getCursorPosition(mut ifd: libc::c_int,
                                           mut ofd: libc::c_int,
                                           mut rows: *mut libc::c_int,
                                           mut cols: *mut libc::c_int)
 -> libc::c_int {
    let mut buf: [libc::c_char; 32] = [0; 32];
    let mut i: libc::c_uint = 0i32 as libc::c_uint;
    if write(ofd,
             b"\x1b[6n\x00" as *const u8 as *const libc::c_char as
                 *const libc::c_void, 4i32 as size_t) != 4i32 as libc::c_long
       {
        return -1i32
    }
    while (i as libc::c_ulong) <
              (::std::mem::size_of::<[libc::c_char; 32]>() as
                   libc::c_ulong).wrapping_sub(1i32 as libc::c_ulong) {
        if read(ifd, buf.as_mut_ptr().offset(i as isize) as *mut libc::c_void,
                1i32 as size_t) != 1i32 as libc::c_long {
            break ;
        }
        if buf[i as usize] as libc::c_int == 'R' as i32 { break ; }
        i = i.wrapping_add(1)
    }
    buf[i as usize] = '\u{0}' as i32 as libc::c_char;
    if buf[0] as libc::c_int != ESC as libc::c_int ||
           buf[1] as libc::c_int != '[' as i32 {
        return -1i32
    }
    if sscanf(buf.as_mut_ptr().offset(2),
              b"%d;%d\x00" as *const u8 as *const libc::c_char, rows, cols) !=
           2i32 {
        return -1i32
    }
    return 0i32;
}
#[no_mangle]
pub unsafe extern "C" fn getWindowSize(mut ifd: libc::c_int,
                                       mut ofd: libc::c_int,
                                       mut rows: *mut libc::c_int,
                                       mut cols: *mut libc::c_int)
 -> libc::c_int {
    let mut ws: winsize =
        winsize{ws_row: 0, ws_col: 0, ws_xpixel: 0, ws_ypixel: 0,};
    if ioctl(1i32, 0x5413i32 as libc::c_ulong, &mut ws as *mut winsize) ==
           -1i32 || ws.ws_col as libc::c_int == 0i32 {
        let mut orig_row: libc::c_int = 0;
        let mut orig_col: libc::c_int = 0;
        let mut retval: libc::c_int = 0;
        retval = getCursorPosition(ifd, ofd, &mut orig_row, &mut orig_col);
        if !(retval == -1i32) {
            if !(write(ofd,
                       b"\x1b[999C\x1b[999B\x00" as *const u8 as
                           *const libc::c_char as *const libc::c_void,
                       12i32 as size_t) != 12i32 as libc::c_long) {
                retval = getCursorPosition(ifd, ofd, rows, cols);
                if !(retval == -1i32) {
                    let mut seq: [libc::c_char; 32] = [0; 32];
                    snprintf(seq.as_mut_ptr(), 32i32 as libc::c_ulong,
                             b"\x1b[%d;%dH\x00" as *const u8 as
                                 *const libc::c_char, orig_row, orig_col);
                    write(ofd, seq.as_mut_ptr() as *const libc::c_void,
                          strlen(seq.as_mut_ptr())) == -1i32 as libc::c_long;
                    return 0i32
                }
            }
        }
        return -1i32
    } else {
        *cols = ws.ws_col as libc::c_int;
        *rows = ws.ws_row as libc::c_int;
        return 0i32
    };
}
#[no_mangle]
pub unsafe extern "C" fn is_separator(mut c: libc::c_int) -> libc::c_int {
    return (c == '\u{0}' as i32 ||
                *(*__ctype_b_loc()).offset(c as isize) as libc::c_int &
                    _ISspace as libc::c_int as libc::c_ushort as libc::c_int
                    != 0 ||
                !strchr(b",.()+-/*=~%[];\x00" as *const u8 as
                            *const libc::c_char, c).is_null()) as libc::c_int;
}
#[no_mangle]
pub unsafe extern "C" fn editorRowHasOpenComment(mut row: *mut erow)
 -> libc::c_int {
    if !(*row).hl.is_null() && (*row).rsize != 0 &&
           *(*row).hl.offset(((*row).rsize - 1i32) as isize) as libc::c_int ==
               3i32 &&
           ((*row).rsize < 2i32 ||
                (*(*row).render.offset(((*row).rsize - 2i32) as isize) as
                     libc::c_int != '*' as i32 ||
                     *(*row).render.offset(((*row).rsize - 1i32) as isize) as
                         libc::c_int != '/' as i32)) {
        return 1i32
    }
    return 0i32;
}
#[no_mangle]
pub unsafe extern "C" fn editorUpdateSyntax(mut row: *mut erow) {
    (*row).hl =
        realloc((*row).hl as *mut libc::c_void, (*row).rsize as libc::c_ulong)
            as *mut libc::c_uchar;
    memset((*row).hl as *mut libc::c_void, 0i32,
           (*row).rsize as libc::c_ulong);
    if E.syntax.is_null() { return }
    let mut i: libc::c_int = 0;
    let mut prev_sep: libc::c_int = 0;
    let mut in_string: libc::c_int = 0;
    let mut in_comment: libc::c_int = 0;
    let mut p: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut keywords: *mut *mut libc::c_char = (*E.syntax).keywords;
    let mut scs: *mut libc::c_char =
        (*E.syntax).singleline_comment_start.as_mut_ptr();
    let mut mcs: *mut libc::c_char =
        (*E.syntax).multiline_comment_start.as_mut_ptr();
    let mut mce: *mut libc::c_char =
        (*E.syntax).multiline_comment_end.as_mut_ptr();
    p = (*row).render;
    i = 0i32;
    while *p as libc::c_int != 0 &&
              *(*__ctype_b_loc()).offset(*p as libc::c_int as isize) as
                  libc::c_int &
                  _ISspace as libc::c_int as libc::c_ushort as libc::c_int !=
                  0 {
        p = p.offset(1);
        i += 1
    }
    prev_sep = 1i32;
    in_string = 0i32;
    in_comment = 0i32;
    if (*row).idx > 0i32 &&
           editorRowHasOpenComment(&mut *E.row.offset(((*row).idx - 1i32) as
                                                          isize)) != 0 {
        in_comment = 1i32
    }
    while *p != 0 {
        if prev_sep != 0 && *p as libc::c_int == *scs.offset(0) as libc::c_int
               && *p.offset(1) as libc::c_int == *scs.offset(1) as libc::c_int
           {
            memset((*row).hl.offset(i as isize) as *mut libc::c_void, 2i32,
                   ((*row).size - i) as libc::c_ulong);
            return
        }
        if in_comment != 0 {
            *(*row).hl.offset(i as isize) = 3i32 as libc::c_uchar;
            if *p as libc::c_int == *mce.offset(0) as libc::c_int &&
                   *p.offset(1) as libc::c_int ==
                       *mce.offset(1) as libc::c_int {
                *(*row).hl.offset((i + 1i32) as isize) =
                    3i32 as libc::c_uchar;
                p = p.offset(2);
                i += 2i32;
                in_comment = 0i32;
                prev_sep = 1i32
            } else { prev_sep = 0i32; p = p.offset(1); i += 1 }
        } else if *p as libc::c_int == *mcs.offset(0) as libc::c_int &&
                      *p.offset(1) as libc::c_int ==
                          *mcs.offset(1) as libc::c_int {
            *(*row).hl.offset(i as isize) = 3i32 as libc::c_uchar;
            *(*row).hl.offset((i + 1i32) as isize) = 3i32 as libc::c_uchar;
            p = p.offset(2);
            i += 2i32;
            in_comment = 1i32;
            prev_sep = 0i32
        } else if in_string != 0 {
            *(*row).hl.offset(i as isize) = 6i32 as libc::c_uchar;
            if *p as libc::c_int == '\\' as i32 {
                *(*row).hl.offset((i + 1i32) as isize) =
                    6i32 as libc::c_uchar;
                p = p.offset(2);
                i += 2i32;
                prev_sep = 0i32
            } else {
                if *p as libc::c_int == in_string { in_string = 0i32 }
                p = p.offset(1);
                i += 1
            }
        } else if *p as libc::c_int == '\"' as i32 ||
                      *p as libc::c_int == '\'' as i32 {
            in_string = *p as libc::c_int;
            *(*row).hl.offset(i as isize) = 6i32 as libc::c_uchar;
            p = p.offset(1);
            i += 1;
            prev_sep = 0i32
        } else if *(*__ctype_b_loc()).offset(*p as libc::c_int as isize) as
                      libc::c_int &
                      _ISprint as libc::c_int as libc::c_ushort as libc::c_int
                      == 0 {
            *(*row).hl.offset(i as isize) = 1i32 as libc::c_uchar;
            p = p.offset(1);
            i += 1;
            prev_sep = 0i32
        } else if *(*__ctype_b_loc()).offset(*p as libc::c_int as isize) as
                      libc::c_int &
                      _ISdigit as libc::c_int as libc::c_ushort as libc::c_int
                      != 0 &&
                      (prev_sep != 0 ||
                           *(*row).hl.offset((i - 1i32) as isize) as
                               libc::c_int == 7i32) ||
                      *p as libc::c_int == '.' as i32 && i > 0i32 &&
                          *(*row).hl.offset((i - 1i32) as isize) as
                              libc::c_int == 7i32 {
            *(*row).hl.offset(i as isize) = 7i32 as libc::c_uchar;
            p = p.offset(1);
            i += 1;
            prev_sep = 0i32
        } else {
            if prev_sep != 0 {
                let mut j: libc::c_int = 0;
                j = 0i32;
                while !(*keywords.offset(j as isize)).is_null() {
                    let mut klen: libc::c_int =
                        strlen(*keywords.offset(j as isize)) as libc::c_int;
                    let mut kw2: libc::c_int =
                        (*(*keywords.offset(j as
                                                isize)).offset((klen - 1i32)
                                                                   as isize)
                             as libc::c_int == '|' as i32) as libc::c_int;
                    if kw2 != 0 { klen -= 1 }
                    if memcmp(p as *const libc::c_void,
                              *keywords.offset(j as isize) as
                                  *const libc::c_void, klen as libc::c_ulong)
                           == 0 &&
                           is_separator(*p.offset(klen as isize) as
                                            libc::c_int) != 0 {
                        memset((*row).hl.offset(i as isize) as
                                   *mut libc::c_void,
                               if kw2 != 0 { 5i32 } else { 4i32 },
                               klen as libc::c_ulong);
                        p = p.offset(klen as isize);
                        i += klen;
                        break ;
                    } else { j += 1 }
                }
                if !(*keywords.offset(j as isize)).is_null() {
                    prev_sep = 0i32;
                    continue ;
                }
            }
            prev_sep = is_separator(*p as libc::c_int);
            p = p.offset(1);
            i += 1
        }
    }
    let mut oc: libc::c_int = editorRowHasOpenComment(row);
    if (*row).hl_oc != oc && (*row).idx + 1i32 < E.numrows {
        editorUpdateSyntax(&mut *E.row.offset(((*row).idx + 1i32) as isize));
    }
    (*row).hl_oc = oc;
}
#[no_mangle]
pub unsafe extern "C" fn editorSyntaxToColor(mut hl: libc::c_int)
 -> libc::c_int {
    match hl {
        2 | 3 => {
            return 36i32
            /* white */
        }
        4 => { return 33i32 }
        5 => { /* yellow */
            return 32i32
        }
        6 => { /* green */
            return 35i32
        }
        7 => { /* magenta */
            return 31i32
        }
        8 => { /* red */
            return 34i32
        }
        _ => { return 37i32 }
    }; /* blu */
}
#[no_mangle]
pub unsafe extern "C" fn editorSelectSyntaxHighlight(mut filename:
                                                         *mut libc::c_char) {
    let mut j: libc::c_uint = 0i32 as libc::c_uint;
    while (j as libc::c_ulong) <
              (::std::mem::size_of::<[editorSyntax; 1]>() as
                   libc::c_ulong).wrapping_div(::std::mem::size_of::<editorSyntax>()
                                                   as libc::c_ulong) {
        let mut s: *mut editorSyntax = HLDB.as_mut_ptr().offset(j as isize);
        let mut i: libc::c_uint = 0i32 as libc::c_uint;
        while !(*(*s).filematch.offset(i as isize)).is_null() {
            let mut p: *mut libc::c_char = 0 as *mut libc::c_char;
            let mut patlen: libc::c_int =
                strlen(*(*s).filematch.offset(i as isize)) as libc::c_int;
            p = strstr(filename, *(*s).filematch.offset(i as isize));
            if !p.is_null() {
                if *(*(*s).filematch.offset(i as isize)).offset(0) as
                       libc::c_int != '.' as i32 ||
                       *p.offset(patlen as isize) as libc::c_int ==
                           '\u{0}' as i32 {
                    E.syntax = s;
                    return
                }
            }
            i = i.wrapping_add(1)
        }
        j = j.wrapping_add(1)
    };
}
#[no_mangle]
pub unsafe extern "C" fn editorUpdateRow(mut row: *mut erow) {
    let mut tabs: libc::c_int = 0i32;
    let mut nonprint: libc::c_int = 0i32;
    let mut j: libc::c_int = 0;
    let mut idx: libc::c_int = 0;
    free((*row).render as *mut libc::c_void);
    j = 0i32;
    while j < (*row).size {
        if *(*row).chars.offset(j as isize) as libc::c_int ==
               TAB as libc::c_int {
            tabs += 1
        }
        j += 1
    }
    (*row).render =
        malloc(((*row).size + tabs * 8i32 + nonprint * 9i32 + 1i32) as
                   libc::c_ulong) as *mut libc::c_char;
    idx = 0i32;
    j = 0i32;
    while j < (*row).size {
        if *(*row).chars.offset(j as isize) as libc::c_int ==
               TAB as libc::c_int {
            let fresh0 = idx;
            idx = idx + 1;
            *(*row).render.offset(fresh0 as isize) =
                ' ' as i32 as libc::c_char;
            while (idx + 1i32) % 8i32 != 0i32 {
                let fresh1 = idx;
                idx = idx + 1;
                *(*row).render.offset(fresh1 as isize) =
                    ' ' as i32 as libc::c_char
            }
        } else {
            let fresh2 = idx;
            idx = idx + 1;
            *(*row).render.offset(fresh2 as isize) =
                *(*row).chars.offset(j as isize)
        }
        j += 1
    }
    (*row).rsize = idx;
    *(*row).render.offset(idx as isize) = '\u{0}' as i32 as libc::c_char;
    editorUpdateSyntax(row);
}
#[no_mangle]
pub unsafe extern "C" fn editorInsertRow(mut at: libc::c_int,
                                         mut s: *mut libc::c_char,
                                         mut len: size_t) {
    if at > E.numrows { return }
    E.row =
        realloc(E.row as *mut libc::c_void,
                (::std::mem::size_of::<erow>() as
                     libc::c_ulong).wrapping_mul((E.numrows + 1i32) as
                                                     libc::c_ulong)) as
            *mut erow;
    if at != E.numrows {
        memmove(E.row.offset(at as isize).offset(1) as *mut libc::c_void,
                E.row.offset(at as isize) as *const libc::c_void,
                (::std::mem::size_of::<erow>() as
                     libc::c_ulong).wrapping_mul((E.numrows - at) as
                                                     libc::c_ulong));
        let mut j: libc::c_int = at + 1i32;
        while j <= E.numrows {
            let ref mut fresh3 = (*E.row.offset(j as isize)).idx;
            *fresh3 += 1;
            j += 1
        }
    }
    (*E.row.offset(at as isize)).size = len as libc::c_int;
    let ref mut fresh4 = (*E.row.offset(at as isize)).chars;
    *fresh4 =
        malloc(len.wrapping_add(1i32 as libc::c_ulong)) as *mut libc::c_char;
    memcpy((*E.row.offset(at as isize)).chars as *mut libc::c_void,
           s as *const libc::c_void, len.wrapping_add(1i32 as libc::c_ulong));
    let ref mut fresh5 = (*E.row.offset(at as isize)).hl;
    *fresh5 = 0 as *mut libc::c_uchar;
    (*E.row.offset(at as isize)).hl_oc = 0i32;
    let ref mut fresh6 = (*E.row.offset(at as isize)).render;
    *fresh6 = 0 as *mut libc::c_char;
    (*E.row.offset(at as isize)).rsize = 0i32;
    (*E.row.offset(at as isize)).idx = at;
    editorUpdateRow(E.row.offset(at as isize));
    E.numrows += 1;
    E.dirty += 1;
}
#[no_mangle]
pub unsafe extern "C" fn editorFreeRow(mut row: *mut erow) {
    free((*row).render as *mut libc::c_void);
    free((*row).chars as *mut libc::c_void);
    free((*row).hl as *mut libc::c_void);
}
#[no_mangle]
pub unsafe extern "C" fn editorDelRow(mut at: libc::c_int) {
    let mut row: *mut erow = 0 as *mut erow;
    if at >= E.numrows { return }
    row = E.row.offset(at as isize);
    editorFreeRow(row);
    memmove(E.row.offset(at as isize) as *mut libc::c_void,
            E.row.offset(at as isize).offset(1) as *const libc::c_void,
            (::std::mem::size_of::<erow>() as
                 libc::c_ulong).wrapping_mul((E.numrows - at - 1i32) as
                                                 libc::c_ulong));
    let mut j: libc::c_int = at;
    while j < E.numrows - 1i32 {
        let ref mut fresh7 = (*E.row.offset(j as isize)).idx;
        *fresh7 += 1;
        j += 1
    }
    E.numrows -= 1;
    E.dirty += 1;
}
#[no_mangle]
pub unsafe extern "C" fn editorRowsToString(mut buflen: *mut libc::c_int)
 -> *mut libc::c_char {
    let mut buf: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut p: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut totlen: libc::c_int = 0i32;
    let mut j: libc::c_int = 0;
    j = 0i32;
    while j < E.numrows {
        totlen += (*E.row.offset(j as isize)).size + 1i32;
        j += 1
    }
    *buflen = totlen;
    totlen += 1;
    buf = malloc(totlen as libc::c_ulong) as *mut libc::c_char;
    p = buf;
    j = 0i32;
    while j < E.numrows {
        memcpy(p as *mut libc::c_void,
               (*E.row.offset(j as isize)).chars as *const libc::c_void,
               (*E.row.offset(j as isize)).size as libc::c_ulong);
        p = p.offset((*E.row.offset(j as isize)).size as isize);
        *p = '\n' as i32 as libc::c_char;
        p = p.offset(1);
        j += 1
    }
    *p = '\u{0}' as i32 as libc::c_char;
    return buf;
}
#[no_mangle]
pub unsafe extern "C" fn editorRowInsertChar(mut row: *mut erow,
                                             mut at: libc::c_int,
                                             mut c: libc::c_int) {
    if at > (*row).size {
        let mut padlen: libc::c_int = at - (*row).size;
        (*row).chars =
            realloc((*row).chars as *mut libc::c_void,
                    ((*row).size + padlen + 2i32) as libc::c_ulong) as
                *mut libc::c_char;
        memset((*row).chars.offset((*row).size as isize) as *mut libc::c_void,
               ' ' as i32, padlen as libc::c_ulong);
        *(*row).chars.offset(((*row).size + padlen + 1i32) as isize) =
            '\u{0}' as i32 as libc::c_char;
        (*row).size += padlen + 1i32
    } else {
        (*row).chars =
            realloc((*row).chars as *mut libc::c_void,
                    ((*row).size + 2i32) as libc::c_ulong) as
                *mut libc::c_char;
        memmove((*row).chars.offset(at as isize).offset(1) as
                    *mut libc::c_void,
                (*row).chars.offset(at as isize) as *const libc::c_void,
                ((*row).size - at + 1i32) as libc::c_ulong);
        (*row).size += 1
    }
    *(*row).chars.offset(at as isize) = c as libc::c_char;
    editorUpdateRow(row);
    E.dirty += 1;
}
#[no_mangle]
pub unsafe extern "C" fn editorRowAppendString(mut row: *mut erow,
                                               mut s: *mut libc::c_char,
                                               mut len: size_t) {
    (*row).chars =
        realloc((*row).chars as *mut libc::c_void,
                ((*row).size as
                     libc::c_ulong).wrapping_add(len).wrapping_add(1i32 as
                                                                       libc::c_ulong))
            as *mut libc::c_char;
    memcpy((*row).chars.offset((*row).size as isize) as *mut libc::c_void,
           s as *const libc::c_void, len);
    (*row).size =
        ((*row).size as libc::c_ulong).wrapping_add(len) as libc::c_int as
            libc::c_int;
    *(*row).chars.offset((*row).size as isize) =
        '\u{0}' as i32 as libc::c_char;
    editorUpdateRow(row);
    E.dirty += 1;
}
#[no_mangle]
pub unsafe extern "C" fn editorRowDelChar(mut row: *mut erow,
                                          mut at: libc::c_int) {
    if (*row).size <= at { return }
    memmove((*row).chars.offset(at as isize) as *mut libc::c_void,
            (*row).chars.offset(at as isize).offset(1) as *const libc::c_void,
            ((*row).size - at) as libc::c_ulong);
    editorUpdateRow(row);
    (*row).size -= 1;
    E.dirty += 1;
}
#[no_mangle]
pub unsafe extern "C" fn editorInsertChar(mut c: libc::c_int) {
    let mut filerow: libc::c_int = E.rowoff + E.cy;
    let mut filecol: libc::c_int = E.coloff + E.cx;
    let mut row: *mut erow =
        if filerow >= E.numrows {
            0 as *mut erow
        } else { &mut *E.row.offset(filerow as isize) as *mut erow };
    if row.is_null() {
        while E.numrows <= filerow {
            editorInsertRow(E.numrows,
                            b"\x00" as *const u8 as *const libc::c_char as
                                *mut libc::c_char, 0i32 as size_t);
        }
    }
    row = &mut *E.row.offset(filerow as isize) as *mut erow;
    editorRowInsertChar(row, filecol, c);
    if E.cx == E.screencols - 1i32 { E.coloff += 1 } else { E.cx += 1 }
    E.dirty += 1;
}
#[no_mangle]
pub unsafe extern "C" fn editorInsertNewline() {
    let mut filerow: libc::c_int = E.rowoff + E.cy;
    let mut filecol: libc::c_int = E.coloff + E.cx;
    let mut row: *mut erow =
        if filerow >= E.numrows {
            0 as *mut erow
        } else { &mut *E.row.offset(filerow as isize) as *mut erow };
    if row.is_null() {
        if filerow == E.numrows {
            editorInsertRow(filerow,
                            b"\x00" as *const u8 as *const libc::c_char as
                                *mut libc::c_char, 0i32 as size_t);
        } else { return }
    } else {
        if filecol >= (*row).size { filecol = (*row).size }
        if filecol == 0i32 {
            editorInsertRow(filerow,
                            b"\x00" as *const u8 as *const libc::c_char as
                                *mut libc::c_char, 0i32 as size_t);
        } else {
            editorInsertRow(filerow + 1i32,
                            (*row).chars.offset(filecol as isize),
                            ((*row).size - filecol) as size_t);
            row = &mut *E.row.offset(filerow as isize) as *mut erow;
            *(*row).chars.offset(filecol as isize) =
                '\u{0}' as i32 as libc::c_char;
            (*row).size = filecol;
            editorUpdateRow(row);
        }
    }
    if E.cy == E.screenrows - 1i32 { E.rowoff += 1 } else { E.cy += 1 }
    E.cx = 0i32;
    E.coloff = 0i32;
}
#[no_mangle]
pub unsafe extern "C" fn editorDelChar() {
    let mut filerow: libc::c_int = E.rowoff + E.cy;
    let mut filecol: libc::c_int = E.coloff + E.cx;
    let mut row: *mut erow =
        if filerow >= E.numrows {
            0 as *mut erow
        } else { &mut *E.row.offset(filerow as isize) as *mut erow };
    if row.is_null() || filecol == 0i32 && filerow == 0i32 { return }
    if filecol == 0i32 {
        filecol = (*E.row.offset((filerow - 1i32) as isize)).size;
        editorRowAppendString(&mut *E.row.offset((filerow - 1i32) as isize),
                              (*row).chars, (*row).size as size_t);
        editorDelRow(filerow);
        row = 0 as *mut erow;
        if E.cy == 0i32 { E.rowoff -= 1 } else { E.cy -= 1 }
        E.cx = filecol;
        if E.cx >= E.screencols {
            let mut shift: libc::c_int = E.screencols - E.cx + 1i32;
            E.cx -= shift;
            E.coloff += shift
        }
    } else {
        editorRowDelChar(row, filecol - 1i32);
        if E.cx == 0i32 && E.coloff != 0 { E.coloff -= 1 } else { E.cx -= 1 }
    }
    if !row.is_null() { editorUpdateRow(row); }
    E.dirty += 1;
}
#[no_mangle]
pub unsafe extern "C" fn editorOpen(mut filename: *mut libc::c_char)
 -> libc::c_int {
    let mut fp: *mut FILE = 0 as *mut FILE;
    E.dirty = 0i32;
    free(E.filename as *mut libc::c_void);
    E.filename = strdup(filename);
    fp = fopen(filename, b"r\x00" as *const u8 as *const libc::c_char);
    if fp.is_null() {
        if *__errno_location() != 2i32 {
            perror(b"Opening file\x00" as *const u8 as *const libc::c_char);
            exit(1i32);
        }
        return 1i32
    }
    let mut line: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut linecap: size_t = 0i32 as size_t;
    let mut linelen: ssize_t = 0;
    loop  {
        linelen = getline(&mut line, &mut linecap, fp);
        if !(linelen != -1i32 as libc::c_long) { break ; }
        if linelen != 0 &&
               (*line.offset((linelen - 1i32 as libc::c_long) as isize) as
                    libc::c_int == '\n' as i32 ||
                    *line.offset((linelen - 1i32 as libc::c_long) as isize) as
                        libc::c_int == '\r' as i32) {
            linelen -= 1;
            *line.offset(linelen as isize) = '\u{0}' as i32 as libc::c_char
        }
        editorInsertRow(E.numrows, line, linelen as size_t);
    }
    free(line as *mut libc::c_void);
    fclose(fp);
    E.dirty = 0i32;
    return 0i32;
}
#[no_mangle]
pub unsafe extern "C" fn editorSave() -> libc::c_int {
    let mut len: libc::c_int = 0;
    let mut buf: *mut libc::c_char = editorRowsToString(&mut len);
    let mut fd: libc::c_int = open(E.filename, 0o2i32 | 0o100i32, 0o644i32);
    if !(fd == -1i32) {
        if !(ftruncate(fd, len as __off_t) == -1i32) {
            if !(write(fd, buf as *const libc::c_void, len as size_t) !=
                     len as libc::c_long) {
                close(fd);
                free(buf as *mut libc::c_void);
                E.dirty = 0i32;
                editorSetStatusMessage(b"%d bytes written on disk\x00" as
                                           *const u8 as *const libc::c_char,
                                       len);
                return 0i32
            }
        }
    }
    free(buf as *mut libc::c_void);
    if fd != -1i32 { close(fd); }
    editorSetStatusMessage(b"Can\'t save! I/O error: %s\x00" as *const u8 as
                               *const libc::c_char,
                           strerror(*__errno_location()));
    return 1i32;
}
#[no_mangle]
pub unsafe extern "C" fn abAppend(mut ab: *mut abuf,
                                  mut s: *const libc::c_char,
                                  mut len: libc::c_int) {
    let mut new: *mut libc::c_char =
        realloc((*ab).b as *mut libc::c_void,
                ((*ab).len + len) as libc::c_ulong) as *mut libc::c_char;
    if new.is_null() { return }
    memcpy(new.offset((*ab).len as isize) as *mut libc::c_void,
           s as *const libc::c_void, len as libc::c_ulong);
    (*ab).b = new;
    (*ab).len += len;
}
#[no_mangle]
pub unsafe extern "C" fn abFree(mut ab: *mut abuf) {
    free((*ab).b as *mut libc::c_void);
}
#[no_mangle]
pub unsafe extern "C" fn editorRefreshScreen() {
    let mut y: libc::c_int = 0;
    let mut r: *mut erow = 0 as *mut erow;
    let mut buf: [libc::c_char; 32] = [0; 32];
    let mut ab: abuf =
        { let mut init = abuf{b: 0 as *mut libc::c_char, len: 0i32,}; init };
    abAppend(&mut ab, b"\x1b[?25l\x00" as *const u8 as *const libc::c_char,
             6i32);
    abAppend(&mut ab, b"\x1b[H\x00" as *const u8 as *const libc::c_char,
             3i32);
    y = 0i32;
    while y < E.screenrows {
        let mut filerow: libc::c_int = E.rowoff + y;
        if filerow >= E.numrows {
            if E.numrows == 0i32 && y == E.screenrows / 3i32 {
                let mut welcome: [libc::c_char; 80] = [0; 80];
                let mut welcomelen: libc::c_int =
                    snprintf(welcome.as_mut_ptr(),
                             ::std::mem::size_of::<[libc::c_char; 80]>() as
                                 libc::c_ulong,
                             b"Kaz\'s editor (keze) --  verison %s\x1b[0K\r\n\x00"
                                 as *const u8 as *const libc::c_char,
                             b"0.8.1\x00" as *const u8 as
                                 *const libc::c_char);
                let mut padding: libc::c_int =
                    (E.screencols - welcomelen) / 2i32;
                if padding != 0 {
                    abAppend(&mut ab,
                             b"~\x00" as *const u8 as *const libc::c_char,
                             1i32);
                    padding -= 1
                }
                loop  {
                    let fresh8 = padding;
                    padding = padding - 1;
                    if !(fresh8 != 0) { break ; }
                    abAppend(&mut ab,
                             b" \x00" as *const u8 as *const libc::c_char,
                             1i32);
                }
                abAppend(&mut ab, welcome.as_mut_ptr(), welcomelen);
            } else {
                abAppend(&mut ab,
                         b"~\x1b[0K\r\n\x00" as *const u8 as
                             *const libc::c_char, 7i32);
            }
        } else {
            r = &mut *E.row.offset(filerow as isize) as *mut erow;
            let mut len: libc::c_int = (*r).rsize - E.coloff;
            let mut current_color: libc::c_int = -1i32;
            if len > 0i32 {
                if len > E.screencols { len = E.screencols }
                let mut c: *mut libc::c_char =
                    (*r).render.offset(E.coloff as isize);
                let mut hl: *mut libc::c_uchar =
                    (*r).hl.offset(E.coloff as isize);
                let mut j: libc::c_int = 0;
                j = 0i32;
                while j < len {
                    if *hl.offset(j as isize) as libc::c_int == 1i32 {
                        let mut sym: libc::c_char = 0;
                        abAppend(&mut ab,
                                 b"\x1b[7m\x00" as *const u8 as
                                     *const libc::c_char, 4i32);
                        if *c.offset(j as isize) as libc::c_int <= 26i32 {
                            sym =
                                ('@' as i32 +
                                     *c.offset(j as isize) as libc::c_int) as
                                    libc::c_char
                        } else { sym = '?' as i32 as libc::c_char }
                        abAppend(&mut ab, &mut sym, 1i32);
                        abAppend(&mut ab,
                                 b"\x1b[0m\x00" as *const u8 as
                                     *const libc::c_char, 4i32);
                    } else if *hl.offset(j as isize) as libc::c_int == 0i32 {
                        if current_color != -1i32 {
                            abAppend(&mut ab,
                                     b"\x1b[39m\x00" as *const u8 as
                                         *const libc::c_char, 5i32);
                            current_color = -1i32
                        }
                        abAppend(&mut ab, c.offset(j as isize), 1i32);
                    } else {
                        let mut color: libc::c_int =
                            editorSyntaxToColor(*hl.offset(j as isize) as
                                                    libc::c_int);
                        if color != current_color {
                            let mut buf_0: [libc::c_char; 16] = [0; 16];
                            let mut clen: libc::c_int =
                                snprintf(buf_0.as_mut_ptr(),
                                         ::std::mem::size_of::<[libc::c_char; 16]>()
                                             as libc::c_ulong,
                                         b"\x1b[%dm\x00" as *const u8 as
                                             *const libc::c_char, color);
                            current_color = color;
                            abAppend(&mut ab, buf_0.as_mut_ptr(), clen);
                        }
                        abAppend(&mut ab, c.offset(j as isize), 1i32);
                    }
                    j += 1
                }
            }
            abAppend(&mut ab,
                     b"\x1b[39m\x00" as *const u8 as *const libc::c_char,
                     5i32);
            abAppend(&mut ab,
                     b"\x1b[0K\x00" as *const u8 as *const libc::c_char,
                     4i32);
            abAppend(&mut ab, b"\r\n\x00" as *const u8 as *const libc::c_char,
                     2i32);
        }
        y += 1
    }
    abAppend(&mut ab, b"\x1b[0K\x00" as *const u8 as *const libc::c_char,
             4i32);
    abAppend(&mut ab, b"\x1b[7m\x00" as *const u8 as *const libc::c_char,
             4i32);
    let mut status: [libc::c_char; 80] = [0; 80];
    let mut rstatus: [libc::c_char; 80] = [0; 80];
    let mut len_0: libc::c_int =
        snprintf(status.as_mut_ptr(),
                 ::std::mem::size_of::<[libc::c_char; 80]>() as libc::c_ulong,
                 b"%.20s - %d lines %s\x00" as *const u8 as
                     *const libc::c_char, E.filename, E.numrows,
                 if E.dirty != 0 {
                     b"(modified)\x00" as *const u8 as *const libc::c_char
                 } else { b"\x00" as *const u8 as *const libc::c_char });
    let mut rlen: libc::c_int =
        snprintf(rstatus.as_mut_ptr(),
                 ::std::mem::size_of::<[libc::c_char; 80]>() as libc::c_ulong,
                 b"%d/%d\x00" as *const u8 as *const libc::c_char,
                 E.rowoff + E.cy + 1i32, E.numrows);
    if len_0 > E.screencols { len_0 = E.screencols }
    abAppend(&mut ab, status.as_mut_ptr(), len_0);
    while len_0 < E.screencols {
        if E.screencols - len_0 == rlen {
            abAppend(&mut ab, rstatus.as_mut_ptr(), rlen);
            break ;
        } else {
            abAppend(&mut ab, b" \x00" as *const u8 as *const libc::c_char,
                     1i32);
            len_0 += 1
        }
    }
    abAppend(&mut ab, b"\x1b[0m\r\n\x00" as *const u8 as *const libc::c_char,
             6i32);
    abAppend(&mut ab, b"\x1b[0K\x00" as *const u8 as *const libc::c_char,
             4i32);
    let mut msglen: libc::c_int =
        strlen(E.statusmsg.as_mut_ptr()) as libc::c_int;
    if msglen != 0 &&
           time(0 as *mut libc::c_void) as libc::c_long - E.statusmsg_time <
               5i32 as libc::c_long {
        abAppend(&mut ab, E.statusmsg.as_mut_ptr(),
                 if msglen <= E.screencols { msglen } else { E.screencols });
    }
    let mut j_0: libc::c_int = 0;
    let mut cx: libc::c_int = 1i32;
    let mut filerow_0: libc::c_int = E.rowoff + E.cy;
    let mut row: *mut erow =
        if filerow_0 >= E.numrows {
            0 as *mut erow
        } else { &mut *E.row.offset(filerow_0 as isize) as *mut erow };
    if !row.is_null() {
        j_0 = E.coloff;
        while j_0 < E.cx + E.coloff {
            if j_0 < (*row).size &&
                   *(*row).chars.offset(j_0 as isize) as libc::c_int ==
                       TAB as libc::c_int {
                cx += 7i32 - cx % 8i32
            }
            cx += 1;
            j_0 += 1
        }
    }
    snprintf(buf.as_mut_ptr(),
             ::std::mem::size_of::<[libc::c_char; 32]>() as libc::c_ulong,
             b"\x1b[%d;%dH\x00" as *const u8 as *const libc::c_char,
             E.cy + 1i32, cx);
    abAppend(&mut ab, buf.as_mut_ptr(),
             strlen(buf.as_mut_ptr()) as libc::c_int);
    abAppend(&mut ab, b"\x1b[?25h\x00" as *const u8 as *const libc::c_char,
             6i32);
    write(1i32, ab.b as *const libc::c_void, ab.len as size_t);
    abFree(&mut ab);
}
#[no_mangle]
pub unsafe extern "C" fn editorSetStatusMessage(mut fmt: *const libc::c_char,
                                                mut args: ...) {
    let mut ap: ::std::ffi::VaListImpl;
    ap = args.clone();
    vsnprintf(E.statusmsg.as_mut_ptr(),
              ::std::mem::size_of::<[libc::c_char; 80]>() as libc::c_ulong,
              fmt, ap.as_va_list());
    E.statusmsg_time = time(0 as *mut libc::c_void) as time_t;
}
#[no_mangle]
pub unsafe extern "C" fn editorFind(mut fd: libc::c_int) {
    let mut query: [libc::c_char; 257] =
        [0i32 as libc::c_char, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let mut qlen: libc::c_int = 0i32;
    let mut last_match: libc::c_int = -1i32;
    let mut find_next: libc::c_int = 0i32;
    let mut saved_hl_line: libc::c_int = -1i32;
    let mut saved_hl: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut saved_cx: libc::c_int = E.cx;
    let mut saved_cy: libc::c_int = E.cy;
    let mut saved_coloff: libc::c_int = E.coloff;
    let mut saved_rowoff: libc::c_int = E.rowoff;
    loop  {
        editorSetStatusMessage(b"Search: %s (Use ESC/Arrows/Enter)\x00" as
                                   *const u8 as *const libc::c_char,
                               query.as_mut_ptr());
        editorRefreshScreen();
        let mut c: libc::c_int = editorReadKey(fd);
        if c == DEL_KEY as libc::c_int || c == CTRL_H as libc::c_int ||
               c == BACKSPACE as libc::c_int {
            if qlen != 0i32 {
                qlen -= 1;
                query[qlen as usize] = '\u{0}' as i32 as libc::c_char
            }
            last_match = -1i32
        } else if c == ESC as libc::c_int || c == ENTER as libc::c_int {
            if c == ESC as libc::c_int {
                E.cx = saved_cx;
                E.cy = saved_cy;
                E.coloff = saved_coloff;
                E.rowoff = saved_rowoff
            }
            if !saved_hl.is_null() {
                memcpy((*E.row.offset(saved_hl_line as isize)).hl as
                           *mut libc::c_void, saved_hl as *const libc::c_void,
                       (*E.row.offset(saved_hl_line as isize)).rsize as
                           libc::c_ulong);
                saved_hl = 0 as *mut libc::c_char
            }
            editorSetStatusMessage(b"\x00" as *const u8 as
                                       *const libc::c_char);
            return
        } else {
            if c == ARROW_RIGHT as libc::c_int ||
                   c == ARROW_DOWN as libc::c_int {
                find_next = 1i32
            } else if c == ARROW_LEFT as libc::c_int ||
                          c == ARROW_UP as libc::c_int {
                find_next = -1i32
            } else if *(*__ctype_b_loc()).offset(c as isize) as libc::c_int &
                          _ISprint as libc::c_int as libc::c_ushort as
                              libc::c_int != 0 {
                if qlen < 256i32 {
                    let fresh9 = qlen;
                    qlen = qlen + 1;
                    query[fresh9 as usize] = c as libc::c_char;
                    query[qlen as usize] = '\u{0}' as i32 as libc::c_char;
                    last_match = -1i32
                }
            }
        }
        if last_match == -1i32 { find_next = 1i32 }
        if find_next != 0 {
            let mut match_0: *mut libc::c_char = 0 as *mut libc::c_char;
            let mut match_offset: libc::c_int = 0i32;
            let mut i: libc::c_int = 0;
            let mut current: libc::c_int = last_match;
            i = 0i32;
            while i < E.numrows {
                current += find_next;
                if current == -1i32 {
                    current = E.numrows - 1i32
                } else if current == E.numrows { current = 0i32 }
                match_0 =
                    strstr((*E.row.offset(current as isize)).render,
                           query.as_mut_ptr());
                if !match_0.is_null() {
                    match_offset =
                        match_0.wrapping_offset_from((*E.row.offset(current as
                                                                        isize)).render)
                            as libc::c_long as libc::c_int;
                    break ;
                } else { i += 1 }
            }
            find_next = 0i32;
            if !saved_hl.is_null() {
                memcpy((*E.row.offset(saved_hl_line as isize)).hl as
                           *mut libc::c_void, saved_hl as *const libc::c_void,
                       (*E.row.offset(saved_hl_line as isize)).rsize as
                           libc::c_ulong);
                saved_hl = 0 as *mut libc::c_char
            }
            if !match_0.is_null() {
                let mut row: *mut erow =
                    &mut *E.row.offset(current as isize) as *mut erow;
                last_match = current;
                if !(*row).hl.is_null() {
                    saved_hl_line = current;
                    saved_hl =
                        malloc((*row).rsize as libc::c_ulong) as
                            *mut libc::c_char;
                    memcpy(saved_hl as *mut libc::c_void,
                           (*row).hl as *const libc::c_void,
                           (*row).rsize as libc::c_ulong);
                    memset((*row).hl.offset(match_offset as isize) as
                               *mut libc::c_void, 8i32,
                           qlen as libc::c_ulong);
                }
                E.cy = 0i32;
                E.cx = match_offset;
                E.rowoff = current;
                E.coloff = 0i32;
                if E.cx > E.screencols {
                    let mut diff: libc::c_int = E.cx - E.screencols;
                    E.cx -= diff;
                    E.coloff += diff
                }
            }
        }
    };
}
#[no_mangle]
pub unsafe extern "C" fn editorMoveCursor(mut key: libc::c_int) {
    let mut filerow: libc::c_int = E.rowoff + E.cy;
    let mut filecol: libc::c_int = E.coloff + E.cx;
    let mut rowlen: libc::c_int = 0;
    let mut row: *mut erow =
        if filerow >= E.numrows {
            0 as *mut erow
        } else { &mut *E.row.offset(filerow as isize) as *mut erow };
    match key {
        1000 => {
            if E.cx == 0i32 {
                if E.coloff != 0 {
                    E.coloff -= 1
                } else if filerow > 0i32 {
                    E.cy -= 1;
                    E.cx = (*E.row.offset((filerow - 1i32) as isize)).size;
                    if E.cx > E.screencols - 1i32 {
                        E.coloff = E.cx - E.screencols + 1i32;
                        E.cx = E.screencols - 1i32
                    }
                }
            } else { E.cx -= 1i32 }
        }
        1001 => {
            if !row.is_null() && filecol < (*row).size {
                if E.cx == E.screencols - 1i32 {
                    E.coloff += 1
                } else { E.cx += 1i32 }
            } else if !row.is_null() && filecol == (*row).size {
                E.cx = 0i32;
                E.coloff = 0i32;
                if E.cy == E.screenrows - 1i32 {
                    E.rowoff += 1
                } else { E.cy += 1i32 }
            }
        }
        1002 => {
            if E.cy == 0i32 {
                if E.rowoff != 0 { E.rowoff -= 1 }
            } else { E.cy -= 1i32 }
        }
        1003 => {
            if filerow < E.numrows {
                if E.cy == E.screenrows - 1i32 {
                    E.rowoff += 1
                } else { E.cy += 1i32 }
            }
        }
        _ => { }
    }
    filerow = E.rowoff + E.cy;
    filecol = E.coloff + E.cx;
    row =
        if filerow >= E.numrows {
            0 as *mut erow
        } else { &mut *E.row.offset(filerow as isize) as *mut erow };
    rowlen = if !row.is_null() { (*row).size } else { 0i32 };
    if filecol > rowlen {
        E.cx -= filecol - rowlen;
        if E.cx < 0i32 { E.coloff += E.cx; E.cx = 0i32 }
    };
}
#[no_mangle]
pub unsafe extern "C" fn editorProcessKeypress(mut fd: libc::c_int) {
    static mut quit_times: libc::c_int = 3i32;
    let mut c: libc::c_int = editorReadKey(fd);
    match c {
        13 => { editorInsertNewline(); }
        17 => {
            if E.dirty != 0 && quit_times != 0 {
                editorSetStatusMessage(b"WARNING!!! File has unsaved changes. Press Ctrl-Q %d more times to quit.\x00"
                                           as *const u8 as
                                           *const libc::c_char, quit_times);
                quit_times -= 1;
                return
            }
            clearScreen();
            exit(0i32);
        }
        19 => { editorSave(); }
        6 => { editorFind(fd); }
        127 | 8 | 1004 => { editorDelChar(); }
        1007 | 1008 => {
            if c == PAGE_UP as libc::c_int && E.cy != 0i32 {
                E.cy = 0i32
            } else if c == PAGE_DOWN as libc::c_int &&
                          E.cy != E.screenrows - 1i32 {
                E.cy = E.screenrows - 1i32
            }
            let mut times: libc::c_int = E.screenrows;
            loop  {
                let fresh10 = times;
                times = times - 1;
                if !(fresh10 != 0) { break ; }
                editorMoveCursor(if c == PAGE_UP as libc::c_int {
                                     ARROW_UP as libc::c_int
                                 } else { ARROW_DOWN as libc::c_int });
            }
        }
        1002 | 1003 | 1000 | 1001 => { editorMoveCursor(c); }
        3 | 12 | 27 => { }
        _ => { editorInsertChar(c); }
    }
    quit_times = 3i32;
}
#[no_mangle]
pub unsafe extern "C" fn editorFileWasModified() -> libc::c_int {
    return E.dirty;
}
#[no_mangle]
pub unsafe extern "C" fn initEditor() {
    E.cx = 0i32;
    E.cy = 0i32;
    E.rowoff = 0i32;
    E.coloff = 0i32;
    E.numrows = 0i32;
    E.row = 0 as *mut erow;
    E.dirty = 0i32;
    E.filename = 0 as *mut libc::c_char;
    E.syntax = 0 as *mut editorSyntax;
    if getWindowSize(0i32, 1i32, &mut E.screenrows, &mut E.screencols) ==
           -1i32 {
        perror(b"Unable to query the screen for size (columns / rows)\x00" as
                   *const u8 as *const libc::c_char);
        exit(1i32);
    }
    E.screenrows -= 2i32;
}
unsafe fn main_0(mut argc: libc::c_int, mut argv: *mut *mut libc::c_char)
 -> libc::c_int {
    if argc != 2i32 {
        fprintf(stderr,
                b"Usage: keze <filename>\n\x00" as *const u8 as
                    *const libc::c_char);
        exit(1i32);
    }
    initEditor();
    editorSelectSyntaxHighlight(*argv.offset(1));
    editorOpen(*argv.offset(1));
    enableRawMode(0i32);
    editorSetStatusMessage(b"Ctrl-S = save | Ctrl-Q = quit | Ctrl-F = find\x00"
                               as *const u8 as *const libc::c_char);
    loop  { editorRefreshScreen(); editorProcessKeypress(0i32); };
}
pub fn main() {
    let mut args: Vec<*mut libc::c_char> = Vec::new();
    for arg in ::std::env::args() {
        args.push(::std::ffi::CString::new(arg).expect("Failed to convert argument into CString.").into_raw());
    };
    args.push(::std::ptr::null_mut());
    unsafe {
        ::std::process::exit(main_0((args.len() - 1) as libc::c_int,
                                    args.as_mut_ptr() as
                                        *mut *mut libc::c_char) as i32)
    }
}
